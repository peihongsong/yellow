import os
from pathlib import Path

BASE_DIR_PATH = Path(__file__).parent.parent
BASE_DIR = str(BASE_DIR_PATH)
PROJECT_NAME = 'YellowDetection: %s'
WECHAT_RECEIVERS = [328]

if os.getenv('WITHOUT_GPU'):
    WITHOUT_GPU = True
else:
    WITHOUT_GPU = False

import logging
import logging.config
import os
import sys
import traceback
from weixin_tools import WeixinTools, WeixinToolsException
from config import inc
from utils.log_module.log_proxy import LogProxy


class DecoratorLogger(object):
    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        logger = LogProxy("bussinessLogger")
        try:
            return self.func(*args, **kwargs)
        except Exception:
            logger.error(traceback.format_exc())


# 未捕获的异常
def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    logger = LogProxy("bussinessLogger")
    logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
    if not issubclass(exc_type, WeixinToolsException):
        obj = WeixinTools(log_filename=inc.BASE_DIR + '/logs/weixin_tools.log')
        obj.send_message("Cluster Monitor New Project Uncaught exception!\n请查看business.log获取详细信息!",
                         to_users=inc.WECHAT_RECEIVERS)


sys.excepthook = handle_exception

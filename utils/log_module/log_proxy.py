import logging.config
import os
import sys
import traceback
import yaml
from weixin_tools import WeixinTools
from config import inc


class LoggerNameIllegalException(Exception):
    def __init__(self, err=''):
        Exception.__init__(self, err)


CURRENT_DIRECTORY = os.path.dirname(os.path.realpath(__file__))


class LogProxy(object):
    _instance = {}

    def __new__(cls, *args, **kwargs):
        if not cls._instance.get(args[0], None):
            cls._instance[args[0]] = super(LogProxy, cls).__new__(cls)
        return cls._instance[args[0]]

    def __init__(self, logger_name, log_dir=inc.BASE_DIR):
        if logger_name not in ["serverLogger"]:
            raise LoggerNameIllegalException
        else:
            # 初始化日志
            logger_config = yaml.load(open(os.path.join(CURRENT_DIRECTORY, 'log_config.yaml')))
            for key in logger_config['handlers']:
                if 'filename' in logger_config['handlers'][key]:
                    logger_config['handlers'][key]['filename'] = os.path.join(log_dir, logger_config['handlers'][key][
                        'filename'])
            logging.config.dictConfig(config=logger_config)
            self.logger = logging.getLogger(logger_name)
            self.weixin = WeixinTools(log_filename=inc.BASE_DIR + '/logs/weixin_tools.log')

    def info(self, message, send_weixin=False):
        if send_weixin:
            self.weixin.send_message(inc.PROJECT_NAME % (message,), inc.WECHAT_RECEIVERS)
        self.logger.info(message)

    def warn(self, message, send_weixin=False):
        if send_weixin:
            self.weixin.send_message(inc.PROJECT_NAME % (message,), inc.WECHAT_RECEIVERS)
        self.logger.warn(message)

    def error(self, message, send_weixin=True):
        if send_weixin:
            self.weixin.send_message(inc.PROJECT_NAME % (message,), inc.WECHAT_RECEIVERS)
        self.logger.error(message)

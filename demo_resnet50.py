import os, re, sys, time
import numpy as np

from keras import backend as K
from keras.applications.imagenet_utils import preprocess_input
from keras.models import load_model
from keras.preprocessing import image
import tensorflow as tf
from pathlib import Path
import pandas as pd
from shutil import copyfile, rmtree
from keras.utils.generic_utils import Progbar


config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.1
session = tf.Session(config=config)
K.set_session(session)


def check_folder(folder):
    if os.path.exists(folder):
        rmtree(folder)
    os.makedirs(folder)

def predict(img_path, model):
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    preds = model.predict(x)
    return preds


attris = ["pornsexy"] # ,'porn', 'sexy','pornsexy']
tvts = ["train", 'valid', 'test']
for attri in attris:
    for tvt in tvts:
        print(f'resnet50:  attri:{attri}, tvt:{tvt}')
        result_folder = f"./result/0203_resnet50_{attri}/{tvt}"
        image_folder = f"/data/open_data/nsfw/links_{attri}_10k/{tvt}"
        model_path = f'0203/0203_resnet50/resnet50_best_{attri}.h5'

        nsfw = list(Path(f'{image_folder}/nsfw/').glob('*.jpg'))
        sfw = list(Path(f'{image_folder}/sfw/').glob('*.jpg'))
        nsfw = [str(p) for p in nsfw]
        sfw = [str(p) for p in sfw]

        print('Loading model:', model_path)
        t0 = time.time()
        model = load_model(model_path)
        t1 = time.time()
        print('Loaded in:', t1-t0)

        bar = Progbar(len(nsfw))
        times = 0
        records_nsfw = []
        for p in nsfw:
            bar.update(times)
            times += 1
            pred = predict(p, model)[0]
            records_nsfw.append({
                'path':p,
                'score':pred[0],
                'true':'nsfw'
            })

        bar = Progbar(len(sfw))
        times = 0
        records_sfw = []
        for p in sfw:
            bar.update(times)
            times += 1
            pred = predict(p, model)[0]
            records_sfw.append({
                'path':p,
                'score':pred[0],
                'true':'sfw'
            })

        df_nsfw = pd.DataFrame(records_nsfw)
        df_sfw = pd.DataFrame(records_sfw)

        th = 0.5
        sum_nsfw = df_nsfw.shape[0]
        sum_sfw = df_sfw.shape[0]
        tp = df_nsfw[df_nsfw['score'] > th].shape[0]
        tn = df_sfw[df_sfw['score'] <= th].shape[0]

        print(f'{tp}/{sum_nsfw}')
        print(f'{tn}/{sum_sfw}')
        acc = (tp + tn) / (sum_nsfw + sum_sfw)
        fp = sum_nsfw - tp
        fn = sum_sfw - tn
        precision = float(tp) / (tp+fp)
        recall = float(tp) / (tp+fn)
        print(f'sum_nsfw: {sum_nsfw}   true prob:{tp}   false prob:{sum_nsfw - tp}')
        print(f'sum_nsfw: {sum_sfw}    true prob:{tn}   false prob:{sum_sfw - tn}')
        print(f'tp:{tp},fp:{fp},tn:{tn},fn:{fn}')
        print(f'precision:{precision:.3f}, recall:{recall:.3f}')
        print(f'Accuracy: {acc:.3f}')
        print(f'{attri},{tvt},{precision:.3f},{recall:.3f},{acc:.3f},{tp},{fp},{tn},{fn}')

        df = pd.concat([df_nsfw,df_sfw])
        df['pred'] = ['nsfw' if s >0.5 else 'sfw' for s in df['score']]
        fp = df[(df['true'] == 'sfw') & (df['pred'] == 'nsfw')]
        fn = df[(df['true'] == 'nsfw') & (df['pred'] == 'sfw')]
        fp_path_list = fp['path'].tolist()
        fn_path_list = fn['path'].tolist()

        check_folder(f'{result_folder}/fp')
        check_folder(f'{result_folder}/fn')

        for p in fp_path_list:
            copyfile(p,f'{result_folder}/fp/{Path(p).name}')
        for p in fn_path_list:
            copyfile(p,f'{result_folder}/fn/{Path(p).name}')

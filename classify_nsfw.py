#!/usr/bin/env python
import sys
import argparse
import tensorflow as tf
sys.path.append("D:code/tensorflow-open_nsfw")
from model import OpenNsfwModel, InputType
from image_utils import create_tensorflow_image_loader
from image_utils import create_yahoo_image_loader
from tools.freeze_graph import freeze_model
import numpy as np
import os
import shutil
from keras.utils.generic_utils import Progbar

IMAGE_LOADER_TENSORFLOW = "tensorflow"
IMAGE_LOADER_YAHOO = "yahoo"

def check_folder(folder):
    if os.path.exists(folder):
        shutil.rmtree(folder)
    os.makedirs(folder)
'''
python classify_nsfw.py -m="/data/peihongs/data/yellow/pretrained_yahoo/open_nsfw-weights.npy" \
--input_files=/data/open_data/nsfw/links_pornsexy_10k/train \
--result_folder=result_folder=result/0202_yahoo_origin_pornsexy/train
'''
# /data/open_data/nsfw/links_porn_10k/test

def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument("--input_files", help="Path to the input image.\
                        Only jpeg images are supported.")

    parser.add_argument("-m", "--model_weights", required=True,
                        help="Path to trained model weights file")

    parser.add_argument("--result_folder", required=True,
                        help="Path to result file")

    parser.add_argument("-l", "--image_loader",
                        default=IMAGE_LOADER_YAHOO,
                        help="image loading mechanism",
                        choices=[IMAGE_LOADER_YAHOO, IMAGE_LOADER_TENSORFLOW])

    parser.add_argument("-i", "--input_type",
                        default=InputType.TENSOR.name.lower(),
                        help="input type",
                        choices=[InputType.TENSOR.name.lower(),
                                 InputType.BASE64_JPEG.name.lower()])

    args = parser.parse_args()

    model = OpenNsfwModel()

    with tf.Session() as sess:
        input_type = InputType[args.input_type.upper()]
        input_tensor = None
        if input_type == InputType.TENSOR:
            model_input = tf.placeholder(tf.float32, shape=[None, 224, 224, 3], name="input")
            input_tensor = model_input
        elif input_type == InputType.BASE64_JPEG:
            from image_utils import load_base64_tensor

            model_input = tf.placeholder(tf.string, shape=(None,), name="input")
            input_tensor = load_base64_tensor(model_input)
        else:
            raise ValueError("invalid input type '{}'".format(input_type))

        model.build(input_tensor=input_tensor, weights_path=args.model_weights, input_type=input_type)

        fn_load_image = None

        if input_type == InputType.TENSOR:
            if args.image_loader == IMAGE_LOADER_TENSORFLOW:
                fn_load_image = create_tensorflow_image_loader(
                    tf.Session(graph=tf.Graph()))
            else:
                fn_load_image = create_yahoo_image_loader()
        elif input_type == InputType.BASE64_JPEG:
            import base64

            def fn_load_image(filename): return np.array(
                [base64.urlsafe_b64encode(open(filename, "rb").read())])

        sess.run(tf.global_variables_initializer())


        
        labels = ['sfw', 'nsfw']
        # labels = ['sfw', 'nsfw']
        # i ,label:  0 sfw
        # i ,label:  1 nsfw
        f = open("tmp.txt", 'w')
        TP, TN, FP, FN = 0, 0, 0, 0
        for label in labels:
            T, F = 0, 0
            T_sub_folder = os.path.join(args.result_folder, label, "T")
            check_folder(T_sub_folder)
            F_sub_folder = os.path.join(args.result_folder, label, "F")
            check_folder(F_sub_folder)
            image_sub_folder = os.path.join(args.input_files, label)
            image_names = os.listdir(image_sub_folder)
            image_names = [os.path.join(image_sub_folder, image_name)
                        for image_name in image_names]
            bar = Progbar(len(image_names))
            times = 0
            for image_name in image_names:
                bar.update(times)
                times += 1
                image = fn_load_image(image_name)

                predictions = sess.run(model.predictions, feed_dict={model_input: image})
                # print(predictions)
                sfw, nsfw = predictions[0]

                prob = nsfw if sfw < nsfw else sfw
                prob_label = "nsfw" if sfw < nsfw else "sfw"
                if prob_label not in labels:
                    print("---------------error---------")
                f.write(f'{image_name}, {prob_label}, {prob} \n')
                if label == prob_label:
                    T += 1
                    basename = os.path.basename(image_name)
                    dst_name = os.path.join(T_sub_folder, basename)
                    shutil.copyfile(image_name, dst_name)
                else:
                    F += 1
                    basename = os.path.basename(image_name)
                    dst_name = os.path.join(F_sub_folder, basename)
                    shutil.copyfile(image_name, dst_name)
            print(f"{label} count: {T+F}, True prob: {T}, percentage:{round(float(T)/ (T+F),2)} False prob:{F}")
            if label == "nsfw":
                TP += T
                FP += F
            else:
                TN += T
                FN += F

        precision = float(TP) / float(TP+FP)
        recall = float(TP) / float(TP+FN)
        accuracy = float(TP+TN)/(TP+TN+FP+FN)
        print(f"TP:{TP}    FP:{FP}    TN:{TN}    FN:{FN}")
        print(f"precision: {round(precision,3)}, recall:{round(recall,3)}, accuracy: {round(accuracy,3)}")



            # print("Results for '{}'".format(args.input_file))
            # print("\tSFW score:\t{}\n\tNSFW score:\t{}".format(*predictions[0]))

        # print('all nodes are:\n')
        # input_graph_def = sess.graph.as_graph_def()
        # node_names = [node.name for node in input_graph_def.node]
        # for x in node_names:
        #     print(x)

        # freeze graph
        # frozen_pb_file = "D:/yahoo_frozen.pb"
        # output_node_names = 'predictions'
        # freeze_model(sess, sess.graph, output_node_names, frozen_pb_file)

        # save to ckpt
        # saver = tf.train.Saver()
        # ckpt_model = "D:/yahoo.ckpt"
        # save_path = saver.save(sess, ckpt_model)
        # print("Model saved in path: %s" % save_path)


if __name__ == "__main__":
    main(sys.argv)

# -*- coding: utf-8 -*-
# @Author  : tuoyu@clubfactory.com
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Activation, Dropout, Flatten, Dense
from keras import callbacks, optimizers
from keras import backend as K
import os
import tensorflow as tf

attri = "porn"
DATA_DIR = f'/data/open_data/nsfw/links_{attri}_10k'
BEST_CKPT = f'0203_mini/mini_best_{attri}.h5'
FINAL_CKPT = f'0203_mini/mini_final_{attri}.h5'
TRAIN_DIR = os.path.join(DATA_DIR, 'train')
VALID_DIR = os.path.join(DATA_DIR, 'valid')

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.4
K.set_session(tf.Session(config=config))

# dimensions of our images.
img_width, img_height = 224,  224

epochs = 30
batch_size = 128

if K.image_data_format() == 'channels_first':
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)

model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(64))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              # optimizer=optimizers.RMSprop(),
              optimizer=optimizers.Adam(lr=0.01),
              metrics=['accuracy'])

datagen = ImageDataGenerator(validation_split=0.2, rescale=1. / 255, horizontal_flip=True)
train_gen = datagen.flow_from_directory(
    directory=TRAIN_DIR,
    target_size=(img_width, img_height),
    subset='training',
    batch_size=batch_size,
    follow_links=True,
    class_mode='binary'
)
val_gen = datagen.flow_from_directory(
    directory=VALID_DIR,
    target_size=(img_width, img_height),
    subset='validation',
    batch_size=batch_size,
    follow_links=True,
    class_mode='binary'
)

earlystopping = callbacks.EarlyStopping(monitor='val_loss', patience=10, verbose=0, mode='auto')
checkpointer = callbacks.ModelCheckpoint(BEST_CKPT, verbose=1, save_best_only=True)

classes = list(iter(train_gen.class_indices))
for c in train_gen.class_indices:
    classes[train_gen.class_indices[c]] = c
model.classes = classes

model.fit_generator(
    train_gen,
    steps_per_epoch=8000 // batch_size,
    epochs=epochs,
    validation_data=val_gen,
    validation_steps=2000 // batch_size,
    callbacks=[earlystopping, checkpointer],
)

model.save_weights(FINAL_CKPT)

#!/usr/bin/env python
# -*- coding: utf-8 -*- \#
# @Author  : peihongsong@clubfactory.com
r"""基于深度学习的同款匹配方法

图像分类
该模块流程如下：
【1】首先导入模型,模型文件为arcface_ckpt。
【2】输入图像,判断图像是否有效
【3】图像预处理
【4】图像输入到模型,进行predict result, 得到图像对应的特征向量
"""

import cv2
import os
import pickle
import shutil
import numpy as np
from path import Path
from typing import Union
from sklearn import preprocessing
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
from tensorflow.python.platform import gfile
from os.path import join, split


VGG_MEAN = [123.68, 116.78, 103.94]


def normal_preprocess(img, w, h):
    if (img.shape[0] != w or img.shape[1] != h):
        img = cv2.resize(img, (w, h), interpolation=cv2.INTER_LINEAR)
    # 图像减去均值
    # img = np.array(img, dtype=np.float32)
    # mean = np.array(VGG_MEAN, dtype=np.float32)
    # image = img - np.array(mean)
    # image = np.array(image, dtype=np.uint8)
    # return image
    return img


class Yahoo:
    def __init__(self, model_path, input_tensor_name="input:0", out_tensor_name="resnet_v2_152/pool5:0", dim=2048, batch_size=200, w=224, h=224):
        self.w = w
        self.h = h
        self.dim = dim
        self.batch_size = batch_size
        self.input_tensor_name = input_tensor_name
        self.out_tensor_name = out_tensor_name
        self.__load_model_pb(model_path)

    def __load_model_save_model(self, model_path):
        '''读入模型，模型文件为saved_model.pb 和 variables文件夹 格式
        '''
        # load saved_model.pb  and variables
        with tf.Session() as sess:
            print('\nRestoring...', model_path)
            tf.saved_model.loader.load(sess, ["train"], model_path)
            self.graph = tf.get_default_graph()
            print("restore done")
            # for n in tf.get_default_graph().as_graph_def().node:
            #     print(n.name)

    def __load_model_ckpt(self, model_path):
        '''读入模型，模型文件为graph.pb 和 model.ckpt格式
        '''
        path, _ = split(model_path)
        graph_pbfile = join(path, "graph.pb")
        graph = tf.Graph()
        graph_def = tf.GraphDef()
        with open(graph_pbfile, "rb") as f:
            graph_def.ParseFromString(f.read())
        with graph.as_default():
            tf.import_graph_def(graph_def)
            # for n in tf.get_default_graph().as_graph_def().node:
            #     print(n.name)

        saver = tf.train.Saver()
        with tf.Session() as sess:
            # Restore variables from disk.
            saver.restore(sess, model_path)
            self.graph = tf.get_default_graph()
            print("restore done")

    def __load_model_pb(self, pb_file):
        '''读入模型，模型文件为frozen.pb
        '''
        with gfile.FastGFile(pb_file, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            tf.import_graph_def(graph_def, name='')
            self.graph = tf.get_default_graph()
            # for n in tf.get_default_graph().as_graph_def().node:
            # if('input' in n.name):
            # print(n.name)

    def __read_cv_img(self, cv_imgs: np.ndarray):
        self.cv_imgs = cv_imgs

    def __generate_data(self,):
        '''图像预处理
        '''
        self.data = np.empty((len(self.cv_imgs), self.w, self.h, 3))
        for idx, img in enumerate(self.cv_imgs):
            if img is None or img.ndim != 3:
                self.data[idx, ] = None
            else:
                # 正常情况下
                img = self.preprocess_single_cv_img(img, self.w, self.h)
                self.data[idx, ] = img

    def __predict(self, ):
        '''计算特征向量
        '''
        config = tf.ConfigProto(allow_soft_placement=True)
        with tf.Session(config=config, graph=self.graph) as sess:
            sess.run(tf.global_variables_initializer())
            self.image = sess.graph.get_tensor_by_name(self.input_tensor_name)
            self.embedding_tensor = sess.graph.get_tensor_by_name(
                self.out_tensor_name)
            embeddings = sess.run(self.embedding_tensor, feed_dict={
                                  self.image: self.data})
        # hash_values = preprocessing.normalize(
        #     embeddings.reshape(len(self.data), self.dim), axis=1)

        return embeddings

    def split_batch(self, iterable, size):
        ret = []
        for index, item in enumerate(iterable):
            ret.append(item)
            if (index + 1) % size == 0:
                yield ret
                ret = []
            self.offset = index
        if ret:
            yield ret

    def calculate_features_from_cv_imgs(self, cv_imgs):
        features = None
        for batch in self.split_batch(cv_imgs, self.batch_size):
            # print(batch)
            self.__read_cv_img(batch)
            self.__generate_data()
            batch_features = self.__predict()
            # print(batch_features.shape)
            features = np.concatenate((features, batch_features), axis=0) if(
                features is not None) else batch_features
        if features is None:
            features = np.array([]).reshape(0, self.dim).astype('float32')
        return features

    def preprocess_single_cv_img(self, cv_img, w, h):
        cv_img = normal_preprocess(cv_img, w, h)
        return cv_img

    def calculate_features_from_image_names(self, image_names):
        id_map = {}
        idx = 0
        cv_imgs = []
        tmp_imgs = [cv2.imread(image_name) for image_name in image_names]
        for sequence, img in enumerate(tmp_imgs):
            if img is None:  # 过滤掉无法读取成功的图
                continue
            cv_imgs.append(img)
            id_map[idx] = sequence  # features中的idx对应于image_names中的idx
            idx += 1
        features = self.calculate_features_from_cv_imgs(cv_imgs)
        return id_map, features

    def check_yellow(self, cv_imgs):
        labels = ['sfw', 'nsfw']
        # 判断images是否是yellow的, 返回label
        # 这里保证images都是好的，channel=3, rgb, is not None的
        prob_labels = []
        scores = self.calculate_features_from_cv_imgs(cv_imgs)
        for score in scores:
            values = list(score)
            prob = max(values)
            prob_label = labels[values.index(prob)]
            prob_labels.append(prob_label)
        return prob_labels


def read_txt(txt):
    image_names = []
    with open(txt, 'r') as f:
        for line in f.readlines():
            line = line.strip("\n")
            # print(line)
            image_names.append(line)
    return image_names


def split_batch(iterable, size):
    ret = []
    for index, item in enumerate(iterable):
        ret.append(item)
        if (index + 1) % size == 0:
            yield ret
            ret = []
    if ret:
        yield ret


def check_folder(folder):
    if os.path.exists(folder):
        shutil.rmtree(folder)
    os.makedirs(folder)

def main():
    attris = ["pornsexy"] # 'porn' ,'sexy','pornsexy']
    tvts = ['test']  # "train", 'valid', 
    weight = "010"
    for attri in attris:
        for tvt in tvts:
            print(f'yahoo_finetune:  attri:{attri}, tvt:{tvt}')
            frozen_pb = f"/data/peihongs/code/yellow/0318/frozen_yahoo_{attri}_{weight}.pb"# "/data/peihongs/code/tensorflow-open_nsfw/yahoo_finetune/frozen_yahoo_finetune_checked.pb" # "/data/peihongs/code/tensorflow-open_nsfw/lenet/frozen_lenet.pb"# "/data/peihongs/code/tensorflow-open_nsfw/yahoo_finetune/frozen_yahoo_finetune_online_data.pb" # frozen_yahoo_finetune.pb"
            result_folder = f"./result/0318_yahoo_{attri}_{weight}/{tvt}"  # "./result/frozen_yahoo_finetune_online_data"
            image_folder = f"/data/open_data/nsfw/links_{attri}_10k/{tvt}"
            num_classes = 2

            batch_size = 50
            input_tensor_name = 'IteratorGetNext:0'  # 'IteratorGetNext:0'
            out_tensor_name = "predictions:0"  # 'vgg_16/fc8/squeezed:0'
            instance = Yahoo(frozen_pb, input_tensor_name=input_tensor_name,
                            out_tensor_name=out_tensor_name, dim=num_classes, batch_size=batch_size, w=224, h=224)


            
            labels = ['sfw', 'nsfw']
            f = open("tmp.txt", 'w')
            TP, TN, FP, FN = 0, 0, 0, 0
            for label in labels:
                T, F = 0, 0
                T_sub_folder = join(result_folder, label, "T")
                check_folder(T_sub_folder)
                F_sub_folder = join(result_folder, label, "F")
                check_folder(F_sub_folder)
                image_sub_folder = join(image_folder, label)
                image_names = os.listdir(image_sub_folder)
                image_names = [join(image_sub_folder, image_name)
                            for image_name in image_names]
                for batch in split_batch(image_names, batch_size):
                    id_map, features = instance.calculate_features_from_image_names(
                        batch)
                    result = [(batch[id_map[idx]], feature)
                            for idx, feature in enumerate(features)]

                    for image_name, score in result:
                        values = list(score)
                        # print(values)
                        # if values[1] > 0.1:
                        #     prob = values[1]
                        #     prob_label = "nsfw"
                        # else:
                        #     prob = values[0]
                        #     prob_label = "sfw"
                        prob = max(values)
                        prob_label = labels[values.index(prob)]
                        if prob_label not in labels:
                            print("---------------error---------")
                        f.write(f'{image_name}, {prob_label}, {prob} \n')
                        if label == prob_label:
                            T += 1
                            basename = os.path.basename(image_name)
                            dst_name = os.path.join(T_sub_folder, basename)
                            # shutil.copyfile(image_name, dst_name)
                        else:
                            F += 1
                            basename = os.path.basename(image_name)
                            dst_name = os.path.join(F_sub_folder, basename)
                            shutil.copyfile(image_name, dst_name)
                if label == "nsfw":
                    TP += T
                    FN += F
                else:
                    TN += T
                    FP += F

                print(
                    f"{label} count: {T+F}, True prob: {T}, percentage:{round(float(T)/ (T+F),2)} False prob:{F}")
            print(f"TP:{TP}    FP:{FP}    TN:{TN}    FN:{FN}")
            precision = float(TP) / float(TP+FP) if TP+FP != 0 else 0
            recall = float(TP) / float(TP+FN) if TP+FN != 0 else 0
            accuracy = float(TP+TN)/(TP+TN+FP+FN) if TP+TN+FP+FN != 0 else 0
            print(f"precision: {round(precision,3)}, recall:{round(recall,3)}, accuracy: {round(accuracy,3)}")
            print(f'{attri},{tvt},{precision:.3f},{recall:.3f},{accuracy:.3f},{TP},{FP},{TN},{FN}')

if __name__ == "__main__":
    main()

'''freeze graph to frozen.pb
'''

import tensorflow as tf
from tensorflow.python.framework.graph_util import convert_variables_to_constants



def freeze_model(sess, graph, output_node_names, frozen_pb_file):
    # 用来将多个ckpt文件合并成同一个
    '''用于将当前graph和weight等模型参数信息冻结到同个文件中，该文件的文件类型为pb
    '''
    print('all nodes are:\n')
    input_graph_def = graph.as_graph_def()
    node_names = [node.name for node in input_graph_def.node]
    for x in node_names:
        print(x)
    sess.run(tf.global_variables_initializer())
    output_graph_def = convert_variables_to_constants(
        sess, input_graph_def, output_node_names.split(','))
    with tf.gfile.GFile(frozen_pb_file, 'wb') as f:
        f.write(output_graph_def.SerializeToString())
    sess.close()

'''将模型训练完，得到的ckpt直接转成frozen.pb
输入：
--ckpt-path
   model/best.ckpt-19340
   包含：
   best.ckpt-19340.data-00000-of-00001
   best.ckpt-19340.meta
   best.ckpt-19340.index
--base_name
--export_base_path

输出：
base_name.pb
frozen_base_name.pb
optimized_base_name.pb
'''

import os
import sys
import argparse

import tensorflow as tf
from tensorflow.python.tools import freeze_graph
from tensorflow.python.tools import optimize_for_inference_lib



def ckpt2frozen(ckpt_path, base_name, export_base_path, input_node_name='input', output_node_name='predictions', do_freeze=True, do_optimize=False):
    as_binary = True
    if as_binary:
        graph_name = base_name + '.pb'
    else:
        graph_name = base_name + '.pbtxt'
    graph_path = os.path.join(export_base_path, graph_name)
    frozen_graph_path = os.path.join(export_base_path,
                                     'frozen_' + graph_name)
    optimized_graph_path = os.path.join(export_base_path,
                                    'optimized_' + graph_name)
    with tf.Session() as sess:
        meta_path = f"{ckpt_path}.meta"
        new_saver = tf.train.import_meta_graph(meta_path)
        new_saver.restore(sess, ckpt_path)
        print("print node")
        # for n in tf.get_default_graph().as_graph_def().node:
        #     print(n.name)
        print("done")
    
        tf.train.write_graph(sess.graph_def, export_base_path, graph_name,
                             as_text=not as_binary)

        print('Graph exported to {}'.format(graph_path))

        if do_freeze:
            print('Freezing graph...')
            freeze_graph.freeze_graph(
                input_graph=graph_path, input_saver='',
                input_binary=as_binary, input_checkpoint=ckpt_path,
                output_node_names=output_node_name,
                restore_op_name='save/restore_all',
                filename_tensor_name='save/Const:0',
                output_graph=frozen_graph_path, clear_devices=True,
                initializer_nodes='')

            print('Frozen graph exported to {}'.format(frozen_graph_path))

            graph_path = frozen_graph_path

        if do_optimize:
            print('Optimizing graph...')
            input_graph_def = tf.GraphDef()

            with tf.gfile.Open(graph_path, 'rb') as f:
                data = f.read()
                input_graph_def.ParseFromString(data)

                output_graph_def =\
                    optimize_for_inference_lib.optimize_for_inference(
                        input_graph_def,
                        [input_node_name],
                        [output_node_name],
                        tf.float32.as_datatype_enum)

                f = tf.gfile.FastGFile(optimized_graph_path, 'wb')
                f.write(output_graph_def.SerializeToString())

                print('Optimized graph exported to {}'
                      .format(optimized_graph_path))

weight = "010"
attri = "pornsexy"
ckpt_path = f"/data/peihongs/code/yellow/0318_{attri}_{weight}/best.ckpt-72"
base_name = f"yahoo_{attri}_{weight}"
export_base_path = "./0318"
ckpt2frozen(ckpt_path, base_name, export_base_path, input_node_name='input', output_node_name='predictions')

# ckpt2frozen(ckpt_path, base_name, export_base_path,  input_node_name='IteratorGetNext', output_node_name='vgg_16/fc8/squeezed')

# ckpt_path = "/data/peihongs/code/tensorflow-open_nsfw/model_cf_comment/best.ckpt-1584"
# base_name = "yahoo_finetune_online_data"
# export_base_path = "./yahoo_finetune"
# ckpt2frozen(ckpt_path, base_name, export_base_path)

# ckpt_path = "/data/peihongs/code/defect/output/model.ckpt-2552"
# base_name = "lenet"
# export_base_path = "./lenet"
# ckpt2frozen(ckpt_path, base_name, export_base_path, input_node_name='input', output_node_name='Predictions/Reshape_1')


# ckpt_path = "/data/peihongs/code/tensorflow-open_nsfw/vgg16/best.ckpt-108"
# base_name = "vgg16"
# export_base_path = "./vgg_0201"
# out_tensor_name = 'vgg_16/fc8/squeezed:0'
# ckpt2frozen(ckpt_path, base_name, export_base_path, input_node_name = 'IteratorGetNext', output_node_name='vgg_16/fc8/squeezed')

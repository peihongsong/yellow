import json
import os
import re
import sys
import time
import numpy as np

from keras import backend as K
from keras.applications.imagenet_utils import preprocess_input
from keras.models import load_model
from keras.preprocessing import image
import tensorflow as tf
from shutil import copyfile, rmtree
from pathlib import Path
import pandas as pd
from keras.utils.generic_utils import Progbar

config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.1
session = tf.Session(config=config)
K.set_session(session)


def check_folder(folder):
    if os.path.exists(folder):
        rmtree(folder)
    os.makedirs(folder)


def predict(img_path, model):
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img) / 255
    x = np.expand_dims(x, axis=0)
    preds = model.predict(x)
    return preds


attris = ["pornsexy"]  # ,'sexy','pornsexy']
tvts = ["train", 'valid', 'test']

for attri in attris:
    model_path = f'0203_mini/mini_best_{attri}.h5'
    print('Loading model:', model_path)
    t0 = time.time()
    model = load_model(model_path)
    t1 = time.time()
    print('Loaded in:', t1-t0)

    for tvt in tvts:
        print(f'mini:  attri:{attri}, tvt:{tvt}')
        RESULT_DIR = f"./result/0203_mini_{attri}/{tvt}"
        DATA_DIR = f"/data/open_data/nsfw/links_{attri}_10k/{tvt}"

        nsfw = list(Path(f'{DATA_DIR}/nsfw/').glob('*.jpg'))
        sfw = list(Path(f'{DATA_DIR}/sfw/').glob('*.jpg'))
        nsfw = [str(p) for p in nsfw]
        sfw = [str(p) for p in sfw]

        records_nsfw = []
        bar = Progbar(len(nsfw))
        times = 0
        for p in nsfw:
            bar.update(times)
            times += 1
            pred = predict(p, model)[0]
            records_nsfw.append({
                'path': p,
                'score': 1-np.squeeze(pred),
                'true': 'nsfw'
            })

        bar = Progbar(len(sfw))
        times = 0
        records_sfw = []
        for p in sfw:
            bar.update(times)
            times += 1
            pred = predict(p, model)[0]
            records_sfw.append({
                'path': p,
                'score': 1-np.squeeze(pred),
                'true': 'sfw'
            })

        df_nsfw = pd.DataFrame(records_nsfw)
        df_sfw = pd.DataFrame(records_sfw)

        th = 0.5

        sum_nsfw = df_nsfw.shape[0]
        sum_sfw = df_sfw.shape[0]
        tp = df_nsfw[df_nsfw['score'] > th].shape[0]
        tn = df_sfw[df_sfw['score'] <= th].shape[0]

        print(f'{tp}/{sum_nsfw}')
        print(f'{tn}/{sum_sfw}')
        acc = (tp + tn) / (sum_nsfw + sum_sfw)
        fn = sum_nsfw - tp
        fp = sum_sfw - tn
        precision = float(tp) / (tp+fp)
        recall = float(tp) / (tp+fn)
        print(
            f'sum_nsfw: {sum_nsfw}   true prob:{tp}   false prob:{sum_nsfw - tp}')
        print(
            f'sum_nsfw: {sum_sfw}    true prob:{tn}   false prob:{sum_sfw - tn}')
        print(f'tp:{tp},fp:{fp},tn:{tn},fn:{fn}')
        print(f'precision:{precision}, recall:{recall}')
        print(f'Accuracy: {acc:.4f}')
        print(f'{attri},{tvt},{precision:.3f},{recall:.3f},{acc:.3f},{tp},{fp},{tn},{fn}')

        df = pd.concat([df_nsfw, df_sfw])

        df['pred'] = ['nsfw' if s > 0.5 else 'sfw' for s in df['score']]
        df = df[['true', 'pred', 'score', 'path']]
        fp = df[(df['true'] == 'sfw') & (df['pred'] == 'nsfw')]
        fn = df[(df['true'] == 'nsfw') & (df['pred'] == 'sfw')]
        fp_path_list = fp['path'].tolist()
        fn_path_list = fn['path'].tolist()

        check_folder(f'{RESULT_DIR}/fp')
        check_folder(f'{RESULT_DIR}/fn')

        for p in fp_path_list:
            copyfile(p, f'{RESULT_DIR}/fp/{Path(p).name}')
        for p in fn_path_list:
            copyfile(p, f'{RESULT_DIR}/fn/{Path(p).name}')

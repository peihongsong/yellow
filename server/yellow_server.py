#!/usr/bin/env python
# -*- coding: utf-8 -*- \#

import re
import io
import json
import _thread
import traceback
import cv2
import requests
import time
import numpy as np
import os
import psutil
from pympler import muppy
from datetime import datetime
from collections import Counter
from PIL import Image
import tensorflow as tf
import tornado.web
from tornado import gen
from tornado.log import access_log
from tornado.httpserver import HTTPServer
from utils.log_module.log_proxy import LogProxy
from demo_nsfw import Yahoo


def imread(path):
    try:
        img = Image.open(path).load()
    except:
        print('ERROR: %s' % path)
        return None
    else:
        return img

def log_func(handler):
    if handler.get_status() < 400:
        log_method = access_log.info
    elif handler.get_status() < 500:
        log_method = access_log.warning
    else:
        log_method = access_log.error

    request_time = 1000.0 * handler.request.request_time()
    log_method("%d %s %s (%s) %s %s %.2fms at %s",
               handler.get_status(), handler.request.method,
               handler.request.uri, handler.request.remote_ip,
               handler.request.body.decode(),
               handler.request.headers["User-Agent"], request_time,
               datetime.now().strftime('%Y-%m-%d %H:%M:%S'))


logger = LogProxy('serverLogger')


class ImageWithYellow(tornado.web.RequestHandler):
    def __init__(self, *args, **kwargs):
        super(ImageWithYellow, self).__init__(*args, **kwargs)


    def prepare(self):
        self.checker = self.application.yellow_filter

    @staticmethod
    def fix_schema(url):
        if url.startswith('//'):
            url = 'http:' + url
        return url

    def download_image(self, url: str):
        if re.match(r'^https?:/{2}\w.+$', url):
            try:
                resp = requests.get(url, timeout=3)
            except Exception:
                logger.info(f"exception resp: {url}")
                resp = None

            if resp and resp.ok:
                img = np.asarray(bytearray(resp.content), dtype='uint8')
                try:
                    img_decode = cv2.imdecode(img, cv2.IMREAD_COLOR)
                except Exception:
                    logger.info(f"img_decode resp: {url}")
                    img_decode = None
                return img_decode
        return None

    def determine_yellow_or_not(self, image_urls, cv_images):
        rt_dicts = {}
        for url in image_urls:
            rt_dicts[url] = {"status": "success", "result": "not_yellow"}
        need_to_test_cv_imgs = []
        need_to_test_urls = []
        # 判断图像是否可用
        for idx, cv_image in enumerate(cv_images):
            if cv_image is None or cv_image.shape[2] != 3 or cv_image.ndim != 3:
                rt_dicts[image_urls[idx]]['status'] = 'failed'
                continue
            need_to_test_cv_imgs.append(cv_image)
            need_to_test_urls.append(image_urls[idx])

        # 跑模型，看效果
        labels = self.checker.check_yellow(need_to_test_cv_imgs)
        for idx, label in enumerate(labels):
            url = need_to_test_urls[idx]
            rt_dicts[url]['result'] = label
           
        return rt_dicts

    def get_memory(self):
        pid = os.getpid()
        py = psutil.Process(pid)
        # memory use in GB...I think
        memory_use = py.memory_info()[0] / 2. ** 30
        all_objects = muppy.get_objects()
        logger.info(
            f'{datetime.now()} memory use:{memory_use}  pid:{pid} object count: {len(all_objects)}')

    @gen.coroutine
    def get(self):
        urls = self.get_arguments('url')
        # urls = url_s_str.split(',')
        start_time = time.time()
        cv_images = [self.download_image(url) for url in urls]
        end_time = time.time()
        cost = end_time - start_time
        logger.info(f"{datetime.now()} DOWNLOAD COST: {cost}s")
        result_dict = {}
        try:
            result_dict.update(self.determine_yellow_or_not(urls, cv_images))
        except Exception as e:
            logger.error(traceback.format_exc(e))
        self.finish(json.dumps(result_dict))
        end_time = time.time()
        cost = end_time - start_time
        logger.info(str(result_dict))
        logger.info(f"{datetime.now()} TOTAL COST: {cost}s")
        self.get_memory()
        return


settings = {
    "log_function": log_func
}

class Application(tornado.web.Application):
    def __init__(self, *args, **kwargs):

        super(Application, self).__init__(
            [
                (r"/", ImageWithYellow),
            ],
            **settings
        )
        # filter初始化
        frozen_pb = "/data/peihongs/code/tensorflow-open_nsfw/0203/frozen_yahoo_pornsexy.pb"
        num_classes = 2
        input_tensor_name = 'IteratorGetNext:0'
        out_tensor_name = 'predictions:0'
        batch_size = 20
        self.yellow_filter = Yahoo(frozen_pb, input_tensor_name=input_tensor_name, out_tensor_name=out_tensor_name, dim=num_classes, batch_size=batch_size, w=224, h=224)


def main():
    application = Application()
    http_server = HTTPServer(
        application, no_keep_alive=True, idle_connection_timeout=20, body_timeout=20)

    http_server.listen(8991)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    main()

import math, json, os, sys

import keras
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.layers import Dense
from keras.models import Model
from keras.optimizers import Adam
from keras.preprocessing import image


# DATA_DIR = '/data/tyu_code/data/nsfw_checked/train_mix_links/'
DATA_DIR = '/data/open_data/nsfw/links_pornsexy_10k'
BEST_CKPT = '0203/0203_resnet50/resnet50_best_pornsexy.h5'
TRAIN_DIR = os.path.join(DATA_DIR, 'train')
VALID_DIR = os.path.join(DATA_DIR, 'valid')
SIZE = (224, 224)
BATCH_SIZE = 128

# ---------------------------------
import tensorflow as tf
import keras.backend.tensorflow_backend as KTF

config = tf.ConfigProto()
# config.gpu_options.allow_growth=True
config.gpu_options.per_process_gpu_memory_fraction = 0.5
session = tf.Session(config=config)
KTF.set_session(session)
# ---------------------------------

if __name__ == "__main__":
    num_train_samples = 8000 #sum([len(files) for r, d, files in os.walk(TRAIN_DIR)])
    num_valid_samples = 2000 #sum([len(files) for r, d, files in os.walk(VALID_DIR)])
#     print(num_train_samples)
    
    num_train_steps = math.floor(num_train_samples/BATCH_SIZE)
    num_valid_steps = math.floor(num_valid_samples/BATCH_SIZE)

    datagen = keras.preprocessing.image.ImageDataGenerator(validation_split=0.2, rescale=1. / 255, horizontal_flip=True)
    train_gen = datagen.flow_from_directory(
        directory=TRAIN_DIR,
        target_size=SIZE,
        subset='training',
        batch_size=BATCH_SIZE,
        follow_links=True,
        class_mode='categorical'
    )
    val_gen = datagen.flow_from_directory(
        directory=VALID_DIR,
        target_size=SIZE,
        subset='validation',
        batch_size=BATCH_SIZE,
        follow_links=True,
        class_mode='categorical'
    )
    
    #     gen = keras.preprocessing.image.ImageDataGenerator()
    #     val_gen = keras.preprocessing.image.ImageDataGenerator(horizontal_flip=True, vertical_flip=True)

    #     batches = gen.flow_from_directory(
    #         TRAIN_DIR, 
    #         target_size=SIZE, 
    #         class_mode='categorical', 
    #         shuffle=True, 
    #         batch_size=BATCH_SIZE
    #     )
    #     val_batches = val_gen.flow_from_directory(VALID_DIR, target_size=SIZE, class_mode='categorical', shuffle=True, batch_size=BATCH_SIZE)

    model = keras.applications.resnet50.ResNet50()
    # model = keras.applications.VGG16()
    classes = list(iter(train_gen.class_indices))
    model.layers.pop()
    for layer in model.layers:
        layer.trainable=False
    last = model.layers[-1].output
    x = Dense(len(classes), activation="softmax")(last)
    finetuned_model = Model(model.input, x)
    # lr = 0.0001
    finetuned_model.compile(optimizer=Adam(lr=0.001), loss='categorical_crossentropy', metrics=['accuracy'])
    for c in train_gen.class_indices:
        classes[train_gen.class_indices[c]] = c
    finetuned_model.classes = classes

    early_stopping = EarlyStopping(patience=10)
    checkpointer = ModelCheckpoint(BEST_CKPT, verbose=1, save_best_only=True)

    finetuned_model.fit_generator(train_gen, steps_per_epoch=num_train_steps, epochs=1000, callbacks=[early_stopping, checkpointer], validation_data=val_gen, validation_steps=num_valid_steps)
    finetuned_model.save('resnet50_final.h5')

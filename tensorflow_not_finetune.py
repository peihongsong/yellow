"""
Example TensorFlow script for finetuning a VGG model on your own data.
Uses tf.contrib.data module which is in release v1.2
Based on PyTorch example from Justin Johnson
(https://gist.github.com/jcjohnson/6e41e8512c17eae5da50aebef3378a4c)
Required packages: tensorflow (v1.2)
Download the weights trained on ImageNet for VGG:
```
wget http://download.tensorflow.org/models/vgg_16_2016_08_28.tar.gz
tar -xvf vgg_16_2016_08_28.tar.gz
rm vgg_16_2016_08_28.tar.gz
```
For this example we will use a tiny dataset of images from the COCO dataset.
We have chosen eight types of animals (bear, bird, cat, dog, giraffe, horse,
sheep, and zebra); for each of these categories we have selected 100 training
images and 25 validation images from the COCO dataset. You can download and
unpack the data (176 MB) by running:
```
wget cs231n.stanford.edu/coco-animals.zip
unzip coco-animals.zip
rm coco-animals.zip
```
The training data is stored on disk; each category has its own folder on disk
and the images for that category are stored as .jpg files in the category folder.
In other words, the directory structure looks something like this:
coco-animals/
  train/
    bear/
      COCO_train2014_000000005785.jpg
      COCO_train2014_000000015870.jpg
      [...]
    bird/
    cat/
    dog/
    giraffe/
    horse/
    sheep/
    zebra/
  val/
    bear/
    bird/
    cat/
    dog/
    giraffe/
    horse/
    sheep/
    zebra/
"""

'''
python tensorflow_finetune.py \
--train_dir=/data/tyu_code/data/nsfw_keras/train \
--val_dir=/data/tyu_code/data/nsfw_keras/valid \
--model_path=/data/peihongs/data/yellow/pretrained_yahoo \
--batch_size=32 \
--best_checkpoint_dir="./model_tmp"


python tensorflow_finetune.py \
--train_dir=/data/peihongs/data/yellow/cf_comment/train \
--val_dir=None \
--model_path=/data/peihongs/data/yellow/pretrained_yahoo \
--batch_size=32 \
--best_checkpoint_dir="./model_cf_comment" \
--use_val=False


python tensorflow_finetune.py \
--train_dir=/data/peihongs/data/yellow/cf_comment_checked_copy/train \
--val_dir=None \
--model_path=/data/peihongs/data/yellow/pretrained_yahoo \
--batch_size=128 \
--best_checkpoint_dir="./model_cf_comment_0131" \
--use_val=False


python tensorflow_not_finetune.py \
--train_dir=/data/tyu_code/data/nsfw_checked/train_mix_links \
--val_dir=None \
--model_path=/data/peihongs/data/yellow/pretrained_yahoo \
--batch_size=128 \
--best_checkpoint_dir="./vgg16_no_augu" \
--use_val=False

'''

import argparse
import os

import tensorflow as tf
import tensorflow.contrib.slim as slim
import tensorflow.contrib.slim.nets
from model import OpenNsfwModel, InputType
from tools.checkmate import BestCheckpointSaver

parser = argparse.ArgumentParser()
parser.register("type", "bool", lambda v: v.lower() == "true")
parser.add_argument("--use_val", nargs="?", const=True, type="bool", default=True, help="use validation data or not.")
parser.add_argument('--train_dir', default='coco-animals/train')
parser.add_argument('--val_dir', default='coco-animals/val')
parser.add_argument('--model_path', default='vgg_16.ckpt', type=str)
parser.add_argument('--batch_size', default=32, type=int)
parser.add_argument('--num_workers', default=4, type=int)
parser.add_argument('--num_epochs1', default=10, type=int)
parser.add_argument('--num_epochs2', default=10, type=int)
parser.add_argument('--learning_rate1', default=1e-3, type=float)
parser.add_argument('--learning_rate2', default=1e-5, type=float)
parser.add_argument('--dropout_keep_prob', default=0.5, type=float)
parser.add_argument('--weight_decay', default=5e-4, type=float)
parser.add_argument('--best_checkpoint_dir', default='./model', type=str)

VGG_MEAN = [123.68, 116.78, 103.94]

of = open("label_0129.txt",'w')
def list_images(directory):
    """
    Get all the images and labels in directory/label/*.jpg
    """
    labels = os.listdir(directory)
    # Sort the labels so that training and validation get them in the same order
    labels.sort()

    files_and_labels = []
    for label in labels:
        for f in os.listdir(os.path.join(directory, label)):
            files_and_labels.append((os.path.join(directory, label, f), label))

    filenames, labels = zip(*files_and_labels)
    filenames = list(filenames)
    labels = list(labels)
    # unique_labels = list(set(labels))
    unique_labels = ['sfw', 'nsfw']

    label_to_int = {}
    for i, label in enumerate(unique_labels):
        print("label: ", i, label)
        label_to_int[label] = i

    labels = [label_to_int[l] for l in labels]
    for filename, label in zip(filenames, labels):
        of.write(f"{filename}, {label} \n")

    return filenames, labels


def check_accuracy(sess, correct_prediction, is_training, dataset_init_op):
    """
    Check the accuracy of the model on either train or val (depending on dataset_init_op).
    """
    # Initialize the correct dataset
    sess.run(dataset_init_op)
    num_correct, num_samples = 0, 0
    while True:
        try:
            correct_pred = sess.run(correct_prediction, {is_training: False})
            num_correct += correct_pred.sum()
            num_samples += correct_pred.shape[0]
        except tf.errors.OutOfRangeError:
            break

    # Return the fraction of datapoints that were correctly classified
    print(f"num_correct:{num_correct}   num_samples:{num_samples}\n")
    acc = float(num_correct) / num_samples
    return acc


def main(args):
    # Get the list of filenames and corresponding list of labels for training et validation
    train_filenames, train_labels = list_images(args.train_dir)
    if args.use_val:
        val_filenames, val_labels = list_images(args.val_dir)
        assert set(train_labels) == set(val_labels),\
            "Train and val labels don't correspond:\n{}\n{}".format(set(train_labels),
                                                                    set(val_labels))

    num_classes = len(set(train_labels))

    # --------------------------------------------------------------------------
    # In TensorFlow, you first want to define the computation graph with all the
    # necessary operations: loss, training op, accuracy...
    # Any tensor created in the `graph.as_default()` scope will be part of `graph`
    graph = tf.Graph()
    with graph.as_default():
        # Standard preprocessing for VGG on ImageNet taken from here:
        # https://github.com/tensorflow/models/blob/master/research/slim/preprocessing/vgg_preprocessing.py
        # Also see the VGG paper for more details: https://arxiv.org/pdf/1409.1556.pdf

        # Preprocessing (for both training and validation):
        # (1) Decode the image from jpg format
        # (2) Resize the image so its smaller side is 256 pixels long
        def _parse_function(filename, label):
            image_string = tf.read_file(filename)
            image_decoded = tf.image.decode_jpeg(image_string, channels=3)          # (1)
            image = tf.cast(image_decoded, tf.float32)

            smallest_side = 256.0
            height, width = tf.shape(image)[0], tf.shape(image)[1]
            height = tf.to_float(height)
            width = tf.to_float(width)

            scale = tf.cond(tf.greater(height, width),
                            lambda: smallest_side / width,
                            lambda: smallest_side / height)
            new_height = tf.to_int32(height * scale)
            new_width = tf.to_int32(width * scale)

            resized_image = tf.image.resize_images(image, [new_height, new_width])  # (2)
            return resized_image, label

        # Preprocessing (for training)
        # (3) Take a random 224x224 crop to the scaled image
        # (4) Horizontally flip the image with probability 1/2
        # (5) Substract the per color mean `VGG_MEAN`
        # Note: we don't normalize the data here, as VGG was trained without normalization
        def training_preprocess(image, label):
            # crop_image = tf.random_crop(image, [224, 224, 3])                       # (3)
            # flip_image = tf.image.random_flip_left_right(crop_image)                # (4)

            # means = tf.reshape(tf.constant(VGG_MEAN), [1, 1, 3])
            # centered_image = flip_image - means                                    # (5)
            centered_image = tf.image.resize_images(image, [224, 224])

            return centered_image, label

        # Preprocessing (for validation)
        # (3) Take a central 224x224 crop to the scaled image
        # (4) Substract the per color mean `VGG_MEAN`
        # Note: we don't normalize the data here, as VGG was trained without normalization
        def val_preprocess(image, label):
            # crop_image = tf.image.resize_image_with_crop_or_pad(image, 224, 224)    # (3)

            # means = tf.reshape(tf.constant(VGG_MEAN), [1, 1, 3])
            # centered_image = crop_image - means                                     # (4)
            centered_image = tf.image.resize_images(image, [224, 224])
            return centered_image, label

        # ----------------------------------------------------------------------
        # DATASET CREATION using tf.contrib.data.Dataset
        # https://github.com/tensorflow/tensorflow/tree/master/tensorflow/contrib/data

        # The tf.contrib.data.Dataset framework uses queues in the background to feed in
        # data to the model.
        # We initialize the dataset with a list of filenames and labels, and then apply
        # the preprocessing functions described above.
        # Behind the scenes, queues will load the filenames, preprocess them with multiple
        # threads and apply the preprocessing in parallel, and then batch the data

        # Training dataset
        train_dataset = tf.data.Dataset.from_tensor_slices((train_filenames, train_labels))
        train_dataset = train_dataset.map(_parse_function, num_parallel_calls=args.num_workers)
            # num_threads=args.num_workers, output_buffer_size=args.batch_size)
        train_dataset = train_dataset.map(training_preprocess, num_parallel_calls=args.num_workers)
            # num_threads=args.num_workers, output_buffer_size=args.batch_size)
        train_dataset = train_dataset.shuffle(buffer_size=40000)  # don't forget to shuffle
        batched_train_dataset = train_dataset.batch(args.batch_size)

        # Validation dataset
        if args.use_val:
            val_dataset = tf.data.Dataset.from_tensor_slices((val_filenames, val_labels))
            val_dataset = val_dataset.map(_parse_function, num_parallel_calls=args.num_workers)
                # num_threads=args.num_workers, output_buffer_size=args.batch_size)
            val_dataset = val_dataset.map(val_preprocess, num_parallel_calls=args.num_workers)
                # num_threads=args.num_workers, output_buffer_size=args.batch_size)
            batched_val_dataset = val_dataset.batch(args.batch_size)


        # Now we define an iterator that can operator on either dataset.
        # The iterator can be reinitialized by calling:
        #     - sess.run(train_init_op) for 1 epoch on the training set
        #     - sess.run(val_init_op)   for 1 epoch on the valiation set
        # Once this is done, we don't need to feed any value for images and labels
        # as they are automatically pulled out from the iterator queues.

        # A reinitializable iterator is defined by its structure. We could use the
        # `output_types` and `output_shapes` properties of either `train_dataset`
        # or `validation_dataset` here, because they are compatible.
        iterator = tf.data.Iterator.from_structure(batched_train_dataset.output_types,
                                                           batched_train_dataset.output_shapes)
        images, labels = iterator.get_next()

        train_init_op = iterator.make_initializer(batched_train_dataset)
        if args.use_val:
            val_init_op = iterator.make_initializer(batched_val_dataset)

        # Indicates whether we are in training or in test mode
        is_training = tf.placeholder(tf.bool)

        # ---------------------------------------------------------------------
        # Now that we have set up the data, it's time to set up the model.
        # For this example, we'll use VGG-16 pretrained on ImageNet. We will remove the
        # last fully connected layer (fc8) and replace it with our own, with an
        # output size num_classes=8
        # We will first train the last layer for a few epochs.
        # Then we will train the entire model on our dataset for a few epochs.

        # Get the pretrained model, specifying the num_classes argument to create a new
        # fully connected replacing the last one, called "vgg_16/fc8"
        # Each model has a different architecture, so "vgg_16/fc8" will change in another model.
        # Here, logits gives us directly the predicted scores we wanted from the images.
        # We pass a scope to initialize "vgg_16/fc8" weights with he_initializer
        vgg = tf.contrib.slim.nets.vgg
        with slim.arg_scope(vgg.vgg_arg_scope(weight_decay=args.weight_decay)):
            logits, _ = vgg.vgg_16(images, num_classes=num_classes, is_training=is_training,
                                   dropout_keep_prob=args.dropout_keep_prob)
        print("------------images-------------",images)
        print("-----------logits--------------",logits)

        # Specify where the model checkpoint is (pretrained weights).
        # model_path = args.model_path
        # assert(os.path.isfile(model_path))

        # Restore only the layers up to fc7 (included)
        # Calling function `init_fn(sess)` will load all the pretrained weights.
        # variables_to_restore = tf.contrib.framework.get_variables_to_restore(exclude=['fc_nsfw'])
        # init_fn = tf.contrib.framework.assign_from_checkpoint_fn(model_path, variables_to_restore)

        # Initialization operation from scratch for the new "fc8" layers
        # `get_variables` will only return the variables whose name starts with the given pattern
        # fc8_variables = tf.contrib.framework.get_variables('fc_nsfw')
        # fc8_init = tf.variables_initializer(fc8_variables)

        # ---------------------------------------------------------------------
        # Using tf.losses, any loss is added to the tf.GraphKeys.LOSSES collection
        # We can then call the total loss easily
        # nsfw:0  sfw:1, 加nsfw的权重
        # weights = tf.Variable(tf.ones(shape=[args.batch_size, 1]))
        # print("------------labels------------",labels)
        # weights = tf.cast(weights, dtype=tf.int32)
        # weights = tf.subtract(weights, labels)
        # weights = tf.multiply(labels, 2)
        tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
        loss = tf.losses.get_total_loss()

        global_step = tf.train.create_global_step()
        # Then we want to finetune the entire model for a few epochs.
        # We run minimize the loss only with respect to all the variables.
        full_optimizer = tf.train.AdamOptimizer(args.learning_rate1)
        full_train_op = full_optimizer.minimize(loss, global_step=global_step)

        # Evaluation metrics
        prediction = tf.to_int32(tf.argmax(logits, 1))
        correct_prediction = tf.equal(prediction, labels)
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

        # tf.get_default_graph().finalize()

    # global_step_tensor = tf.Variable(10, trainable=False, name='global_step')

    # --------------------------------------------------------------------------
    # Now that we have built the graph and finalized it, we define the session.
    # The session is the interface to *run* the computational graph.
    # We can call our training operations with `sess.run(train_op)` for instance
    session_config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
    session_config.gpu_options.allow_growth = True
    # session_config.gpu_options.per_process_gpu_memory_fraction = 0.5
    with tf.Session(config=session_config, graph=graph) as sess:
        # init_fn(sess)  # load the pretrained weights
        # sess.run(fc8_init)  # initialize the new fc8 layer
        sess.run(tf.global_variables_initializer())
        # sess.run(global_step_tensor.initializer)
        sess.run(train_init_op)

        best_ckpt_saver = BestCheckpointSaver(
            save_dir=args.best_checkpoint_dir,
            num_to_keep=3,
            maximize=True
            )

        # Train the entire model for a few more epochs, continuing with the *same* weights.
        for epoch in range(args.num_epochs2):
            print('Starting epoch %d / %d' % (epoch + 1, args.num_epochs2))
            
            # print("------label-------------")
            # sess.run(labels)
            while True:
                try:
                    _ = sess.run(full_train_op, {is_training: True})
                except tf.errors.OutOfRangeError:
                    break

            # Check accuracy on the train and val sets every epoch
            train_acc = check_accuracy(sess, correct_prediction, is_training, train_init_op)
            print('Train accuracy: %f' % train_acc)
            if args.use_val:
                val_acc = check_accuracy(sess, correct_prediction, is_training, val_init_op)
                print('Val accuracy: %f\n' % val_acc)
            best_ckpt_saver.handle(train_acc, sess, global_step)

if __name__ == '__main__':
    args = parser.parse_args()
    main(args)
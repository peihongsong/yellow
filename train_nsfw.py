'''分类模型finetune
具体而言：
【1】超参数设置：模型训练过程中可能用到的参数
【2】模型硬件配置
【3】生成tfrecord
【4】基本模型结构，根据现有模型的frozen.pb, 得到模型结构，以及输入，输出，loss函数等
【5】数据预处理部分
【6】数据后处理部分
【7】定义模型
【8】模型训练
'''

import tensorflow as tf
slim = tf.contrib.slim


# class Train():
#     def __init__(self, args):
#         # 模型训练过程
#         print("begin process")
#         self.print_hype_parameters(args)
#         self.Config_model_deploy(args)
#         self.select_dataset(args)
#         self.select_network(args, is_training=True)
#         self.select_preprocess(args, is_training=True)
#         self.create_data_provider(args)
#         self.define_model(args)
#         self.train(args)
#         print("done")

#     def print_hype_parameters(args):
#         # 打印超参数
#         pass

#     def Config_model_deploy(self, args):  # 1
#         # 配置模型所处的系统环境
#         # 模型配置，cpu等的配置
#         self.deploy_config = model_deploy.DeploymentConfig(
#             num_clones=args.num_clones,
#             clone_on_cpu=args.clone_on_cpu,
#             replica_id=args.task,
#             num_replicas=args.worker_replicas,
#             num_ps_tasks=args.num_ps_tasks)

#         # Create global_step
#         # 创建设备
#         with tf.device(self.deploy_config.variables_device()):
#             self.global_step = slim.create_global_step()

#     def select_dataset(args):
#         # 选择数据集
#         pass


#!/usr/bin/env python
import sys
import argparse
import tensorflow as tf
sys.path.append("D:code/tensorflow-open_nsfw")
from model import OpenNsfwModel, InputType
from image_utils import create_tensorflow_image_loader
from image_utils import create_yahoo_image_loader
from tools.freeze_graph import freeze_model
import numpy as np


IMAGE_LOADER_TENSORFLOW = "tensorflow"
IMAGE_LOADER_YAHOO = "yahoo"


# Define the model function (following TF Estimator Template)
def model_fn(logit, labels, mode, learning_rate):
    
    # Build the neural network
    # Because Dropout have different behavior at training and prediction time, we
    # need to create 2 distinct computation graphs that still share the same weights.
    # Predictions
    pred_classes = tf.argmax(logit, axis=1)
    pred_probas = tf.nn.softmax(logit)
    
    # If prediction mode, early return
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode, predictions=pred_classes) 
        
    # Define loss and optimizer
    loss_op = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=logit, labels=tf.cast(labels, dtype=tf.int32)))
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(loss_op, global_step=tf.train.get_global_step())
    
    # Evaluate the accuracy of the model
    acc_op = tf.metrics.accuracy(labels=labels, predictions=pred_classes)
    
    # TF Estimators requires to return a EstimatorSpec, that specify
    # the different ops for training, evaluating, ...
    estim_specs = tf.estimator.EstimatorSpec(
      mode=mode,
      predictions=pred_classes,
      loss=loss_op,
      train_op=train_op,
      eval_metric_ops={'accuracy': acc_op})

    return estim_specs

def main(argv):
    parser = argparse.ArgumentParser()

    parser.add_argument("input_file", help="Path to the input image.\
                        Only jpeg images are supported.")

    parser.add_argument("-m", "--model_weights", required=True,
                        help="Path to trained model weights file")

    parser.add_argument("-l", "--image_loader",
                        default=IMAGE_LOADER_YAHOO,
                        help="image loading mechanism",
                        choices=[IMAGE_LOADER_YAHOO, IMAGE_LOADER_TENSORFLOW])

    parser.add_argument("-i", "--input_type",
                        default=InputType.TENSOR.name.lower(),
                        help="input type",
                        choices=[InputType.TENSOR.name.lower(),
                                 InputType.BASE64_JPEG.name.lower()])

    args = parser.parse_args()

    model = OpenNsfwModel()

    with tf.Session() as sess:

        input_type = InputType[args.input_type.upper()]
        model.build(weights_path=args.model_weights, input_type=input_type)

        fn_load_image = None

        if input_type == InputType.TENSOR:
            if args.image_loader == IMAGE_LOADER_TENSORFLOW:
                fn_load_image = create_tensorflow_image_loader(tf.Session(graph=tf.Graph()))
            else:
                fn_load_image = create_yahoo_image_loader()
        elif input_type == InputType.BASE64_JPEG:
            import base64
            fn_load_image = lambda filename: np.array([base64.urlsafe_b64encode(open(filename, "rb").read())])

        sess.run(tf.global_variables_initializer())

        image = fn_load_image(args.input_file)

        predictions = \
            sess.run(model.predictions,
                     feed_dict={model.input: image})

        # print('all nodes are:\n')
        # input_graph_def = sess.graph.as_graph_def()
        # node_names = [node.name for node in input_graph_def.node]
        # for x in node_names:
        #     print(x)

        # freeze graph
        frozen_pb_file = "D:/yahoo_frozen.pb"
        output_node_names = 'predictions'
        freeze_model(sess, sess.graph, output_node_names, frozen_pb_file)

        # model.logits

        
        # Build the Estimator
        model = tf.estimator.Estimator(model_fn)
        # Define the input function for training
        input_fn = tf.estimator.inputs.numpy_input_fn(
            x={'images': mnist.train.images}, y=mnist.train.labels,
            batch_size=batch_size, num_epochs=None, shuffle=True)
        # Train the Model
        model.train(input_fn, steps=num_steps)



if __name__ == "__main__":
    main(sys.argv)
